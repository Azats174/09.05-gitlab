# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM centos:latest

RUN yum update -y && yum install python3 flask flask-jsonpify flask-restful -y
RUN mkdir /python_api
WORKDIR /python_api
COPY python-api.py /python_api/python-api.py


CMD ["python3","python-api.py"]
